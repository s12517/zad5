package tests;


	import static org.junit.Assert.*;

	import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

	import org.junit.Test;

	import domain.Address;
import domain.Permission;
import domain.Person;
import domain.Role;
import domain.User;
import service.UserService;

	public class UserServiceTest {

		@Test(expected=NullPointerException.class)
		public void col_services_should_throw_exception_if_null_is_entered_as_a_parameter(){
			UserService.findUsersWhoHaveMoreThanOneAddress(null);
		}

		@Test
		public void col_services_should_return_empty_list_if_there_is_no_result(){
			
			User u = new User();
			Person p = new Person();
			u.setPersonDetails(p);
			List<User> users = new ArrayList<User>();
			users.add(u);
			
			List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);
			
			assertTrue(result.size()==0);
		}
		
		@Test
		public void col_services_should_return_proper_result_if_there_exists_user_with_more_than_1_address()
		{

			User u = new User();
			User userWith2Addresses = new User();
			Person p = new Person();
			Person p2 = new Person();
			Address a1 = new Address();
			Address a2 = new Address();
			Address a3 = new Address();
			p.getAddresses().add(a1);
			p2.getAddresses().add(a3);
			p2.getAddresses().add(a2);
			u.setPersonDetails(p);
			userWith2Addresses.setPersonDetails(p2);
			
			List<User> users = new ArrayList<User>();
			users.add(u);
			users.add(userWith2Addresses);

			List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);
			
			assertTrue(result.size()==1);
			assertSame(result.get(0), userWith2Addresses);
		}
		
		@Test
		public void col_services_should_ignore_users_without_person_details(){
			User u = new User();
			User userWith2Addresses = new User();
			Person p = new Person();
			Person p2 = new Person();
			Address a1 = new Address();
			Address a2 = new Address();
			Address a3 = new Address();
			p.getAddresses().add(a1);
			p2.getAddresses().add(a3);
			p2.getAddresses().add(a2);
			u.setPersonDetails(p);
			User userWithoutPersonDetails = new User();
			userWith2Addresses.setPersonDetails(p2);

			List<User> users = new ArrayList<User>();
			users.add(u);
			users.add(userWith2Addresses);
			users.add(userWithoutPersonDetails);

			List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);

			assertTrue(result.size()==1);
			assertSame(result.get(0), userWith2Addresses);
		}
		
		@Test(expected=NullPointerException.class)
		public void exception_if_null_is_entered_as_a_parameter(){
			UserService.findOldestPerson(null);
		}

		@Test
		public void empty_list_of_names_if_there_is_no_result(){
			
			User u = new User();
			Person p = new Person();
			u.setPersonDetails(p);
			List<User> users = new ArrayList<User>();
			users.add(u);
			
			<User> result = UserService.findOldestPerson(users);
			
			assertTrue(result.size()==0);
		}
		
		@Test
		public void proper_result_if_oldest_person_is_found()
		{
			List<String> phoneNumbers = new ArrayList<String>();
						
			List<String> joesPhoneNumbers = new ArrayList<String>();
			joesPhoneNumbers.add("4355252345");
			List<Address> joesAddresses = new ArrayList<Address>();
			joesAddresses.add(new Address("paczkowa", 32, 15, "Elblag", "82-300", "Poland"));
			List<Permission> permissions = new ArrayList<Permission>();
			permissions.add(new Permission("cheese"));
			Role joesRole = new Role("jakasRola", permissions);
			Person joe = new Person("Joe", "Grubovsky", joesPhoneNumbers, joesAddresses, joesRole, 16);
			
			List<String> ewasPhoneNumbers = new ArrayList<String>();
			joesPhoneNumbers.add("45645455");
			List<Address> ewasAddresses = new ArrayList<Address>();
			joesAddresses.add(new Address("grzybowa", 32, 15, "Elblagowo", "80-300", "Poland"));
			List<Permission> ewaspermissions = new ArrayList<Permission>();
			ewaspermissions.add(new Permission("destroy"));
			Role ewasRole = new Role("wazna", permissions);
			Person ewa = new Person("Ewa", "Vienna", ewasPhoneNumbers, ewasAddresses, ewasRole, 26);
			
			List<String> wojteksPhoneNumbers = new ArrayList<String>();
			wojteksPhoneNumbers.add("65645657");
			List<Address> wojteksAddresses = new ArrayList<Address>();
			wojteksAddresses.add(new Address("fairy", 32, 15, "Olsztyn", "10-343", "Poland"));
			List<Permission> wojtekspermissions = new ArrayList<Permission>();
			wojtekspermissions.add(new Permission("cry"));
			Role wojteksRole = new Role("niewazna", permissions);
			Person wojtek = new Person("Wojtek", "Gula", wojteksPhoneNumbers, wojteksAddresses, wojteksRole, 2);
			
			assertEquals(ewa, UserService.findOldestPerson(Arrays.asList(
					joe,
					wojtek
			)));
		}
		
		@Test
		public void empty_list_if_there_is_no_result(){
			
			User u = new User();
			Person p = new Person();
			u.setPersonDetails(p);
			List<User> users = new ArrayList<User>();
			users.add(u);
			
			User result = UserService.findUserWithLongestUsername(users);
			
			assertTrue(((List<User>) result).size()==0);
		}
		
		@Test
		public void proper_result_if_longest_username_is_found()
		{
			
			List<String> joesPhoneNumbers = new ArrayList<String>();
			joesPhoneNumbers.add("4355252345");
			List<Address> joesAddresses = new ArrayList<Address>();
			joesAddresses.add(new Address("paczkowa", 32, 15, "Elblag", "82-300", "Poland"));
			List<Permission> permissions = new ArrayList<Permission>();
			permissions.add(new Permission("cheese"));
			Role joesRole = new Role("jakasRola", permissions);
			Person user1Details = new Person("Joe", "Grubovsky", joesPhoneNumbers, joesAddresses, joesRole, 16);
			User user1 = new User("tamburynka", "chleb", user1Details);
			
			
			List<String> ewasPhoneNumbers = new ArrayList<String>();
			joesPhoneNumbers.add("45645455");
			List<Address> ewasAddresses = new ArrayList<Address>();
			joesAddresses.add(new Address("grzybowa", 32, 15, "Elblagowo", "80-300", "Poland"));
			List<Permission> ewaspermissions = new ArrayList<Permission>();
			ewaspermissions.add(new Permission("destroy"));
			Role ewasRole = new Role("wazna", permissions);
			Person user2Details = new Person("Ewa", "Vienna", ewasPhoneNumbers, ewasAddresses, ewasRole, 26);
			User user2 = new User("alelehg", "krrrry", user2Details);
			
			
			List<String> wojteksPhoneNumbers = new ArrayList<String>();
			wojteksPhoneNumbers.add("65645657");
			List<Address> wojteksAddresses = new ArrayList<Address>();
			wojteksAddresses.add(new Address("fairy", 32, 15, "Olsztyn", "10-343", "Poland"));
			List<Permission> wojtekspermissions = new ArrayList<Permission>();
			wojtekspermissions.add(new Permission("cry"));
			Role wojteksRole = new Role("niewazna", permissions);
			Person user3Details = new Person("Wojtek", "Gula", wojteksPhoneNumbers, wojteksAddresses, wojteksRole, 2);
			User user3 = new User("nmnhg", "ojgds", user3Details);
			
			assertEquals(user1, UserService.findUserWithLongestUsername(Arrays.asList(
					user2,
					user3
			)));
		}
		
		@Test
		public void empty_list_of_names_and_surnames_if_there_is_no_result(){
			
			User u = new User();
			Person p = new Person();
			u.setPersonDetails(p);
			List<User> users = new ArrayList<User>();
			users.add(u);
			
			String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
			
			assertTrue(((String User) result).length()==0);
		}
		
		@Test
		public void proper_result_if_people_above_18_found()
		{
			
			List<String> joesPhoneNumbers = new ArrayList<String>();
			joesPhoneNumbers.add("4355252345");
			List<Address> joesAddresses = new ArrayList<Address>();
			joesAddresses.add(new Address("paczkowa", 32, 15, "Elblag", "82-300", "Poland"));
			List<Permission> permissions = new ArrayList<Permission>();
			permissions.add(new Permission("cheese"));
			Role joesRole = new Role("jakasRola", permissions);
			Person joe = new Person("Joe", "Grubovsky", joesPhoneNumbers, joesAddresses, joesRole, 16);
			
			List<String> ewasPhoneNumbers = new ArrayList<String>();
			joesPhoneNumbers.add("45645455");
			List<Address> ewasAddresses = new ArrayList<Address>();
			joesAddresses.add(new Address("grzybowa", 32, 15, "Elblagowo", "80-300", "Poland"));
			List<Permission> ewaspermissions = new ArrayList<Permission>();
			ewaspermissions.add(new Permission("destroy"));
			Role ewasRole = new Role("wazna", permissions);
			Person ewa = new Person("Ewa", "Vienna", ewasPhoneNumbers, ewasAddresses, ewasRole, 26);
			
			List<String> wojteksPhoneNumbers = new ArrayList<String>();
			wojteksPhoneNumbers.add("65645657");
			List<Address> wojteksAddresses = new ArrayList<Address>();
			wojteksAddresses.add(new Address("fairy", 32, 15, "Olsztyn", "10-343", "Poland"));
			List<Permission> wojtekspermissions = new ArrayList<Permission>();
			wojtekspermissions.add(new Permission("cry"));
			Role wojteksRole = new Role("niewazna", permissions);
			Person wojtek = new Person("Wojtek", "Gula", wojteksPhoneNumbers, wojteksAddresses, wojteksRole, 2);
			
			List<String> krzychsPhoneNumbers = new ArrayList<String>();
			krzychsPhoneNumbers.add("656546565");
			List<Address> krzychsAddresses = new ArrayList<Address>();
			wojteksAddresses.add(new Address("Mohoho", 32, 15, "Piekarnia", "10-666", "Poland"));
			List<Permission> krzychspermissions = new ArrayList<Permission>();
			krzychspermissions.add(new Permission("bake"));
			Role krzychsRole = new Role("chlebowa", permissions);
			Person krzych = new Person("Krzych", "Piekarz", krzychsPhoneNumbers, krzychsAddresses, krzychsRole, 33);
			
			
			
			assertEquals("Ewa Vienna, Krzych Piekarz", UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(Arrays.asList(
					joe,
					ewa,
					wojtek,
					krzych
			)));
		}
		
		@Test
		public void empty_list_of_permissions_if_there_is_no_result(){
			
			User u = new User();
			Person p = new Person();
			u.setPersonDetails(p);
			List<User> users = new ArrayList<User>();
			users.add(u);
			
			List<User> result = UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users);
			
			assertTrue(result.size()==0);
		}
		
		@Test
		public void proper_result_if_people_with_names_starting_with_A_found()
		{
			
			List<String> annasPhoneNumbers = new ArrayList<String>();
			annasPhoneNumbers.add("4355252345");
			List<Address> annasAddresses = new ArrayList<Address>();
			annasAddresses.add(new Address("paczkowa", 32, 15, "Elblag", "82-300", "Poland"));
			List<Permission> permissions = new ArrayList<Permission>();
			permissions.add(new Permission("cheese"));
			Role annasRole = new Role("jakasRola", permissions);
			Person anna = new Person("Anna", "Grubovsky", annasPhoneNumbers, annasAddresses, annasRole, 16);
			
			List<String> ewasPhoneNumbers = new ArrayList<String>();
			ewasPhoneNumbers.add("45645455");
			List<Address> ewasAddresses = new ArrayList<Address>();
			ewasAddresses.add(new Address("grzybowa", 32, 15, "Elblagowo", "80-300", "Poland"));
			List<Permission> ewaspermissions = new ArrayList<Permission>();
			ewaspermissions.add(new Permission("destroy"));
			Role ewasRole = new Role("wazna", permissions);
			Person ewa = new Person("Ewa", "Vienna", ewasPhoneNumbers, ewasAddresses, ewasRole, 26);
			
			List<String> areksPhoneNumbers = new ArrayList<String>();
			areksPhoneNumbers.add("65645657");
			List<Address> areksAddresses = new ArrayList<Address>();
			areksAddresses.add(new Address("fairy", 32, 15, "Olsztyn", "10-343", "Poland"));
			List<Permission> arekspermissions = new ArrayList<Permission>();
			arekspermissions.add(new Permission("cry"));
			Role areksRole = new Role("niewazna", permissions);
			Person arek = new Person("Arek", "Gula", areksPhoneNumbers, areksAddresses, areksRole, 2);
			
			List<String> krzychsPhoneNumbers = new ArrayList<String>();
			krzychsPhoneNumbers.add("656546565");
			List<Address> krzychsAddresses = new ArrayList<Address>();
			krzychsAddresses.add(new Address("Mohoho", 32, 15, "Piekarnia", "10-666", "Poland"));
			List<Permission> krzychspermissions = new ArrayList<Permission>();
			krzychspermissions.add(new Permission("bake"));
			Role krzychsRole = new Role("chlebowa", permissions);
			Person krzych = new Person("Krzych", "Piekarz", krzychsPhoneNumbers, krzychsAddresses, krzychsRole, 33);
			
			assertEquals(Arrays.asList(
					"jakasRola",
					"niewazna"
			), UserService.getSortedPermissionsOfUsersWithNameStartingWithA(Arrays.asList(
					anna,
					ewa,
					arek,
					krzych
			)));
		}
		
		@Test
		public void empty_list_of_User_if_there_is_no_result(){
			
			User u = new User();
			Person p = new Person();
			u.setPersonDetails(p);
			List<User> users = new ArrayList<User>();
			users.add(u);
			
			List<User> result = UserService.partitionUserByUnderAndOver18(users);
			
			assertTrue(result.size()==0);
		}
		
		@Test
		public void proper_result_of_grouping_by_age_above_18_or_less()
		{
			
			List<String> annasPhoneNumbers = new ArrayList<String>();
			annasPhoneNumbers.add("4355252345");
			List<Address> annasAddresses = new ArrayList<Address>();
			annasAddresses.add(new Address("paczkowa", 32, 15, "Elblag", "82-300", "Poland"));
			List<Permission> permissions = new ArrayList<Permission>();
			permissions.add(new Permission("destroy"));
			Role annasRole = new Role("wazna", permissions);
			Person anna = new Person("Anna", "Grubovsky", annasPhoneNumbers, annasAddresses, annasRole, 16);
			
			List<String> ewasPhoneNumbers = new ArrayList<String>();
			ewasPhoneNumbers.add("45645455");
			List<Address> ewasAddresses = new ArrayList<Address>();
			ewasAddresses.add(new Address("grzybowa", 32, 15, "Elblagowo", "80-300", "Poland"));
			List<Permission> ewaspermissions = new ArrayList<Permission>();
			ewaspermissions.add(new Permission("destroy"));
			Role ewasRole = new Role("wazna", permissions);
			Person ewa = new Person("Ewa", "Vienna", ewasPhoneNumbers, ewasAddresses, ewasRole, 26);
			
			List<String> areksPhoneNumbers = new ArrayList<String>();
			areksPhoneNumbers.add("65645657");
			List<Address> areksAddresses = new ArrayList<Address>();
			areksAddresses.add(new Address("fairy", 32, 15, "Olsztyn", "10-343", "Poland"));
			List<Permission> arekspermissions = new ArrayList<Permission>();
			arekspermissions.add(new Permission("bake"));
			Role areksRole = new Role("niewazna", permissions);
			Person arek = new Person("Arek", "Gula", areksPhoneNumbers, areksAddresses, areksRole, 2);
			
			List<String> krzychsPhoneNumbers = new ArrayList<String>();
			krzychsPhoneNumbers.add("656546565");
			List<Address> krzychsAddresses = new ArrayList<Address>();
			krzychsAddresses.add(new Address("Mohoho", 32, 15, "Piekarnia", "10-666", "Poland"));
			List<Permission> krzychspermissions = new ArrayList<Permission>();
			krzychspermissions.add(new Permission("bake"));
			Role krzychsRole = new Role("niewazna", permissions);
			Person krzych = new Person("Krzych", "Piekarz", krzychsPhoneNumbers, krzychsAddresses, krzychsRole, 33);
			
			Map<Boolean, List<User>> map = new Map();
			map.put(true, Arrays.asList(ewa, krzych));
			map.put(false, Arrays.asList(anna, arek));
			
			assertEquals(map, UserService.partitionUserByUnderAndOver18(Arrays.asList(
					anna,
					ewa,
					arek,
					krzych
			)));
	}
