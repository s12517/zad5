package service;

import domain.Person;
import domain.Role;
import domain.User;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class UserService {
	

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
    	
    	users.stream()

        .filter(user -> user.getPersonDetails().getAddresses().size() > 1);
		return users;
    	
		}

    public static User findOldestPerson(List<User> users) {
    	
		return users.stream()
				
				.max(
						Comparator.comparing(user -> user.getPersonDetails().getAge())
						)
				.get();
		}

    public static User findUserWithLongestUsername(List<User> users) {
    	
    	return users.stream()
    			.max(
    					Comparator.comparing(user -> user.getName().length())
    					)
    			.get();
    	
    	
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
        
    	return users.stream()
    			  .filter(user -> user.getPersonDetails().getAge() > 18)
    			  .map(user -> user.getPersonDetails().getName() + " " + user.getPersonDetails().getSurname()  )
    			  .collect(Collectors.joining(", "));
    	
		
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
        
    	return users.stream()
    	.filter( p-> user.getName().toLowerCase().charAt(0) == 'a'
		.sorted((permission1, permission2) -> permission1.compareTo(permission2)
	    .map(permission -> permission.getName().forEach(System.out::println));
   }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
       
    	users.stream()
		.filter(user-> user.getPersonDetails().getSurname().toLowerCase().charAt(0) == 's')
		.map(user -> user.getName().toUpperCase())
		.forEach(System.out::println);
  }

  public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
	            
	 return users.stream()
	
	 .filter( user -> user.getPersonDetails() != null &&
			user.getPersonDetails().getRole() != null)
			
			.collect(Collectors.groupingBy(
						user -> user.getPersonDetails().getRole()
				));
    }

  public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
        
	  return users.stream()
	  .collect(Collectors.partitioningBy(user -> user.getPersonDetails().getAge() >= 18));
    }
}
